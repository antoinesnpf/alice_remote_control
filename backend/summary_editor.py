from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import copy

class SummaryEditor :

    def __init__(self, dataframe, mode = 'NM', path = "", name = None) :


        self.mode = mode

        if mode == 'NM' :
            self.dataframe = dataframe
        elif mode == 'GP':
            self.dataframe = [[u] for u in dataframe]
        else :
            print('given mode if not correct')

        self.path = path
        self.date = datetime.now()
        if name is None :
            self.name = "summary_{}".format(self.date)[:-10]
            self.name = self.name[: -3] + 'h' + self.name[-2 :]
            self.name += '.txt'

        else :
            self.name = name
        self.mode = mode

    def get_text_matrix_NM(self):

        text_matrix = [[]]
        index = 0

        input_parameter_names = self.dataframe[0][0].names

        text_matrix[index].append("Nit")

        for parameter in input_parameter_names :
            text_matrix[index].append(parameter)

        text_matrix[index].append("Label")
        text_matrix[index].append("Mean_error")
        text_matrix[index].append("Std_error")

        for current_point in sorted(self.dataframe[0], key = lambda aliceparam : aliceparam.id[0]) :

            index += 1
            text_matrix.append([])

            text_matrix[index].append(str(current_point.id[-1]))

            for coord in current_point.cd :
                text_matrix[index].append(num2str(coord, 2))

            text_matrix[index].append(current_point.label)
            text_matrix[index].append(num2str(current_point.mean_error, 1))
            text_matrix[index].append(num2str(current_point.std_error, 1))

        for parameter_set in self.dataframe[1:]  :

            index += 1
            text_matrix.append([])

            current_point = sorted(parameter_set, key = lambda aliceparam : aliceparam.id[0])[-1]

            text_matrix[index].append(str(current_point.id[-1]))

            for coord in current_point.cd :
                text_matrix[index].append(num2str(coord, 2))

            text_matrix[index].append(current_point.label)
            text_matrix[index].append(num2str(current_point.mean_error, 1))
            text_matrix[index].append(num2str(current_point.std_error, 1))

        # setting the length of the element of the text matrix for a good looking summary
        length_vector = []

        n, p = len(text_matrix), len(text_matrix[0])

        for j in range(p) :
            length_vector.append(len(text_matrix[0][j]))

        for i in range(n) :
            for j in range(p) :
                if length_vector[j] < len(text_matrix[i][j]) :
                    length_vector[j] = len(text_matrix[i][j])

        for i in range(n) :
            for j in range(p) :
                text_matrix[i][j] = relenght(text_matrix[i][j], length_vector[j])

        self.text_matrix = text_matrix


    def save(self) :

        # adding the header of the file

        lines = []
        lines.append("SUMMARY FILE\n")
        lines.append("date : {}".format(self.date)[:-10] + "\n")
        lines[-1] += "\n"
        lines.append("optimization mode : {}\n".format('Nelder-Mead'))
        lines[-1] += "\n"

        lines.append("PARAMETERS :\n" )
        for i, name in enumerate(self.dataframe[0][0].names) :
            lines.append(relenght(str(i), 2) +' | ' + name + '\n')
            lines.append('\tmin : {}\n'.format(self.dataframe[0][0].minimums[i]))
            lines.append('\tmax : {}\n'.format(self.dataframe[0][0].maximums[i]))
        lines[-1] += "\n"

        lines.append("HYPERPARAMETERS :\n" )

        selection = self.dataframe[0][0].selection
        hypernames = self.dataframe[0][0].all_names
        size = max([len(name) for name in hypernames])
        for i,name in enumerate(hypernames) :
            if i not in selection :
                lines.append(relenght(str(i), 3) +' | ' + relenght(name, size) + ' : {}\n'.format(self.dataframe[0][0].all_values[i]))
        lines[-1] += "\n"

        lines.append("RESULTS : \n")

        #getting the text_matrix
        self.get_text_matrix_NM()

        #writing the text matrix into the lines

        n, p = len(self.text_matrix), len(self.text_matrix[0])
        for i in range(n) :
            for j in range(p) :
                if j == 0 :
                    if i == 0 :
                        lines.append(self.text_matrix[i][j] + '\t')
                    else :
                        lines.append('\n' + self.text_matrix[i][j] + '\t')

                else :
                    lines[-1] += self.text_matrix[i][j] + '\t'



        self.lines = lines

        # writting the file
        file = open(self.path + self.name, 'w')
        file.writelines(lines)
        file.close()

    def plot(self) :

        les_x = []
        les_y = []

        dataframe = copy.copy(self.dataframe)
        dataframe[0].sort(key = lambda aliceparam : aliceparam.id[0])
        for current_point in dataframe[0] :
            les_x.append(current_point.id[0])
            les_y.append(current_point.mean_error)

        for parameter_set in dataframe :
            current_point = sorted(parameter_set, key = lambda aliceparam : aliceparam.id[0])[-1]
            les_x.append(current_point.id[0])
            les_y.append(current_point.mean_error)


        les_y2 = [np.min(les_y[0:u]) for u in range(1, len(les_y) + 1)]

        plt.figure()
        plt.plot(les_x, les_y, "o")
        plt.plot(les_x, les_y2, "-")
        plt.title('results for optimization of {}'.format(self.date)[:-10] + ' using {}'.format(self.mode))
        plt.xlabel('iteration number')
        plt.ylabel('cost function')
        plt.show()

    def save_plot(self):

        les_x = []
        les_y = []

        dataframe = copy.copy(self.dataframe)
        dataframe[0].sort(key = lambda AP : AP.id[0])
        for current_point in dataframe[0] :
            les_x.append(current_point.id[0])
            les_y.append(current_point.mean_error)

        for parameter_set in dataframe :
            current_point = sorted(parameter_set, key = lambda aliceparam : aliceparam.id[0])[-1]
            les_x.append(current_point.id[0])
            les_y.append(current_point.mean_error)


        les_y2 = [np.min(les_y[0:u]) for u in range(1, len(les_y) + 1)]

        plt.figure()
        plt.plot(les_x, les_y, "o")
        plt.plot(les_x, les_y2, "-")
        plt.title('results for optimization of {}'.format(self.date)[:-10] + ' using {}'.format(self.mode))
        plt.xlabel('iteration number')
        plt.ylabel('cost function')
        plt.savefig(self.path + self.name[:-4] + '.png')



def num2str(number, k = 2) :
    '''
    converts number into a string with k significant figures
    ex : (1.2, 3) -> '1.200'
         (1.2345, 3) -> '1.234'
    '''


    int_part = int(number)

    sign = 'pos'
    if int_part < 0 :
        sign = 'neg'
        number = - number
        int_part = int(number)

    decimal_part = int(round(number - int_part, k )*10**k)

    decimal_part = str(decimal_part)
    while len(decimal_part) < k :
        decimal_part += "0"

    if sign == 'neg' :
        res = '-' + str(int_part) + "." + decimal_part

    else :
        res = str(int_part) + "." + decimal_part

    return res


def relenght(phrase, k ) :

    assert len(phrase)<= k
    res = phrase + (k - len(phrase))*" "
    return res

def load_data(name) :
    with open(name, 'r') as file :
        lines = file.readlines()
        lines.reverse()

        i = 0
        while lines[i][0] != 'N' :
            j = 0
            nit = ''
            while lines[i][j] != ' ' or lines[i][j] != '\t' :
                nit += lines[i][j]
                j += 1
            nit = int(nit)
            print(nit)
            i += 1




#load_data('summary_2020-12-07 17h43.txt')
