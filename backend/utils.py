import random as rd

def random_seed(min, max) :

    assert len(max) == len(min)


    res = []
    for M,m in zip(max, min) :
        assert m < M
        res.append(m + (M-m)*rd.random())

    return res

