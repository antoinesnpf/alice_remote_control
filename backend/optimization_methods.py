import numpy as np
import copy
import time
import sklearn.gaussian_process as gp
from math import sqrt
from optimization_main import AliceParameters
from signal_handler import handler


class Optimizer :

    def __init__(self,
                 selection,
                 initial_set,
                 parameter_minimums,
                 parameter_maximums,
                 all_parameters_names,
                 all_parameters_values,
                 nb_iter_optimization,
                 nb_iter_alice,
                 input_file_name="In_Pulses.txt",
                 output_file_name="Output.txt",
                 path="") :

        self.selection = selection
        self.initial_set = initial_set
        self.parameter_minimums = parameter_minimums
        self.parameter_maximums = parameter_maximums
        self.all_parameters_names = all_parameters_names
        self.all_parameters_values = all_parameters_values
        self.nb_iter_optimization = nb_iter_optimization
        self.nb_iter_alice = nb_iter_alice
        self.input_file_name = input_file_name
        self.output_file_name = output_file_name
        self.path = path

        self.hyperparameters_GP = {"kernel" : gp.kernels.ConstantKernel(1.0, (1e-1, 1e3)) * gp.kernels.RBF(10.0, (1e-3, 1e3)),
                                   "n_restarts_optimizer" : 10,
                                   "alpha" : 1.0,
                                   "beta" : 0.5}

        self.hyperparameters_NM = {"alpha" : 1,
                                   "gamma": 2,
                                   "ro" : 0.5,
                                   "sigma" : 0.5}


    def run(self, mode = 'NM') :

        assert mode in ['GP', 'NM']

        if mode == 'GP' :

            return optimization_GP( self.selection,
                                    self.initial_set,
                                    self.parameter_minimums,
                                    self.parameter_maximums,
                                    self.all_parameters_names,
                                    self.all_parameters_values,
                                    self.nb_iter_optimization,
                                    self.nb_iter_alice,
                                    self.input_file_name,
                                    self.output_file_name,
                                    self.path,
                                    **self.hyperparameters_GP)

        elif mode == 'NM' :
            return optimization_NM( self.selection,
                                    self.initial_set,
                                    self.parameter_minimums,
                                    self.parameter_maximums,
                                    self.all_parameters_names,
                                    self.all_parameters_values,
                                    self.nb_iter_optimization,
                                    self.nb_iter_alice,
                                    self.input_file_name,
                                    self.output_file_name,
                                    self.path,
                                    **self.hyperparameters_NM)


############ GP ##############

def optimization_GP(selection,
                    initial_set,
                    parameter_minimums,
                    parameter_maximums,
                    all_parameters_names,
                    all_parameters_values,
                    nb_iter_optimization,
                    nb_iter_alice,
                    input_file_name="In_Pulses.txt",
                    output_file_name="Output.txt",
                    path="",
                    **kwargs) :

    '''
    This function optimizes the cold_atom experiment with Gaussian Process regression

    :param selection: list of ints, indexes of the parameters to be optimized
    :param initial_set: list of lists of floats. Each sublist represent a set of parameters values for initialisation of NM.
    :param parameter_minimums: list of the minimums of the parameters. This is used to keep the parameters values in healthy boundaries.
    :param parameter_maximums: list of the maximums of the parameters. This is used to keep the parameters values in healthy boundaries.
    :param all_parameters_names: list of strings, names of all the parameters
    :param all_parameters_names: list of floats, values of all the parameters
    :param nb_iter_optimization: number of iteration of the Nelder_Meal algorithm
    :param nb_iter_alice: number of experiments run to average the cost function corresponding to 1 set of parameters
    :param input_file_name: file name pointed to Alice
    :param output_file_name: file name pointed to Alice
    :param path: points to the directory where the above files will be created

    :return: dataframe. 2-D array of AliceParameters objects.
    An AliceParam object represents a set of parameters
    and the nb_iter_alice experiments that have been run with their values
    See more on the wiki of the repository
    '''


    # initialisation

    initial_set = [np.array(x) for x in initial_set]

    res = []

    c = 1
    print("INITIALISATION {}/{}".format(c, len(initial_set)))

    x0 = initial_set[0]
    res.append(AliceParameters(selection,
                               x0,
                               parameter_minimums,
                               parameter_maximums,
                               all_parameters_names,
                               all_parameters_values,
                               nb_iter_alice,
                               label="init",
                               input_file_name=input_file_name,
                               output_file_name=output_file_name,
                               path=path,
                               init=True))

    for x in initial_set[1:] :

        if handler.SIGINT:
            return res

        c += 1
        print("INITIALISATION {}/{}".format(c, len(initial_set)))
        res.append(AliceParameters(selection,
                                   x,
                                   parameter_minimums,
                                   parameter_maximums,
                                   all_parameters_names,
                                   all_parameters_values,
                                   nb_iter_alice,
                                   label="init",
                                   input_file_name=input_file_name,
                                   output_file_name=output_file_name,
                                   path=path))

    # optimisation

    X_te = test_set(parameter_minimums, parameter_maximums, 20)  # test points

    for k in range(nb_iter_optimization) :

        if handler.SIGINT:
            return res

        print("OPTIMISATION {}/{}".format(k+1, nb_iter_optimization))

        X_tr = [] # training points | shape [# points, # features]
        y_tr = [] # training labels  | shape [# points]

        for aliceparam in res :
            X_tr.append(aliceparam.cd)
            y_tr.append(aliceparam.mean_error)

        model = gp.GaussianProcessRegressor(kernel=kwargs["kernel"], n_restarts_optimizer=kwargs["n_restarts_optimizer"], alpha=kwargs["alpha"], normalize_y=True)
        model.fit(X_tr, y_tr)  # training the model


        params = model.kernel_.get_params()   # get hyperparameters of the model
        y_pred, std = model.predict(X_te, return_std=True) # predictions for the test points

        y_mean = np.mean(y_pred)


        # finding the next point to assess

        beta = kwargs["beta"]
        selector = [-(y - y_mean) + beta * to for (y,to) in zip(y_pred, std)]
        index = np.argmax(selector)

        x = X_te[index]
        y = y_pred[index]
        print('next test point :', x)
        print('its predicted cost :', y)

        X_te.pop(index) # removing the possibility of testing this point again

        res.append(AliceParameters(selection,
                                   x,
                                   parameter_minimums,
                                   parameter_maximums,
                                   all_parameters_names,
                                   all_parameters_values,
                                   nb_iter_alice,
                                   label="GP",
                                   input_file_name=input_file_name,
                                   output_file_name=output_file_name,
                                   path=path))

    return res


def test_set(parameter_minimums, parameter_maximums, size) :

    def recursive_generator(L) :

        if len(L) == 1 :
            return [[x] for x in L[0]]

        else :

            res = []
            for x in L[0] :
                for y in recursive_generator(L[1:]) :
                    res.append([x] + y)

            return res


    L = []

    for m,M in zip(parameter_minimums, parameter_maximums) :
        L.append(np.linspace(m, M, size))

    return recursive_generator(L)



#################### NM #########################

def optimization_NM_init(selection,
                      initial_set,
                      parameter_minimums,
                      parameter_maximums,
                      all_parameters_names,
                      all_parameters_values,
                      nb_iter_alice,
                      input_file_name="In_Pulses.txt",
                      output_file_name="Output.txt",
                      path="") :

    initial_set = [np.array(x) for x in initial_set]
    current_set = []
    dataframe = []
    # initialisation of the scripts for Alice
    c = 1
    print("INITIALISATION {}/{}".format(c, len(initial_set)))

    x0 = initial_set[0]
    current_set.append(AliceParameters(selection,
                                       x0,
                                       parameter_minimums,
                                       parameter_maximums,
                                       all_parameters_names,
                                       all_parameters_values,
                                       nb_iter_alice,
                                       label="init",
                                       input_file_name=input_file_name,
                                       output_file_name=output_file_name,
                                       path=path,
                                       init=True))


    for x in initial_set[1:] :

        if handler.SIGINT:
            return dataframe, current_set

        c += 1
        print("INITIALISATION {}/{}".format(c, len(initial_set)))
        current_set.append(AliceParameters(selection,
                                           x,
                                           parameter_minimums,
                                           parameter_maximums,
                                           all_parameters_names,
                                           all_parameters_values,
                                           nb_iter_alice,
                                           label="init",
                                           input_file_name=input_file_name,
                                           output_file_name=output_file_name,
                                           path=path))

    current_set.sort(key = lambda aliceparam : aliceparam.mean_error) # sorted list of vectors according to vector.error values
    dataframe.append(copy.copy(current_set))

    return dataframe, current_set


def optimization_NM_step(dataframe,
                      current_set,
                      selection,
                      initial_set,
                      parameter_minimums,
                      parameter_maximums,
                      all_parameters_names,
                      all_parameters_values,
                      nb_iter_alice,
                      input_file_name="In_Pulses.txt",
                      output_file_name="Output.txt",
                      path="",
                      **kwargs) :


    N = len(initial_set)
    x0 = sum([vector.cd for vector in current_set[:-1]])
    x0 = x0 / (N-1)

    alpha = kwargs["alpha"]
    gamma = kwargs["gamma"]
    ro = kwargs["ro"]
    sigma = kwargs["sigma"]

    # reflexion

    xn = current_set[-1].cd
    xr = x0+alpha*(x0-xn)
    vr = AliceParameters(selection,
                         xr,
                         parameter_minimums,
                         parameter_maximums,
                         all_parameters_names,
                         all_parameters_values,
                         nb_iter_alice,
                         label="reflexion",
                         input_file_name=input_file_name,
                         output_file_name=output_file_name,
                         path=path)

    if current_set[0].mean_error <= vr.mean_error and vr.mean_error < current_set[-2].mean_error :
        current_set[-1] = vr

    # expansion

    elif vr.mean_error < current_set[0].mean_error :
        xe = x0 + gamma * (xr - x0)
        ve = AliceParameters(selection,
                             xe,
                             parameter_minimums,
                             parameter_maximums,
                             all_parameters_names,
                             all_parameters_values,
                             nb_iter_alice,
                             label="expansion",
                             input_file_name=input_file_name,
                             output_file_name=output_file_name,
                             path=path)

        if ve.mean_error < vr.mean_error :
            current_set[-1] = ve
        else :
            current_set[-1] = vr

    # contraction

    elif vr.mean_error >= current_set[-2].mean_error :
        xc = x0 + ro * (xn - x0)
        vc = AliceParameters(selection,
                             xc,
                             parameter_minimums,
                             parameter_maximums,
                             all_parameters_names,
                             all_parameters_values,
                             nb_iter_alice,
                             label="contraction",
                             input_file_name=input_file_name,
                             output_file_name=output_file_name,
                             path=path)

        if vc.mean_error < current_set[-1].mean_error :
            current_set[-1] = vc

        # shrink

        else :
            for i in range(N-1) :
                xi = current_set[i].cd
                x1 = current_set[0].cd
                xi = x1 + sigma*(xi - x1)
                current_set[i] =  AliceParameters(selection,
                                                  xi,
                                                  parameter_minimums,
                                                  parameter_maximums,
                                                  all_parameters_names,
                                                  all_parameters_values,
                                                  nb_iter_alice,
                                                  label="shrink",
                                                  input_file_name=input_file_name,
                                                  output_file_name=output_file_name,
                                                  path=path)


    current_set.sort(key = lambda aliceparam : aliceparam.mean_error)

    dataframe.append(copy.copy(current_set))

    return dataframe, current_set


def optimization_NM(selection,
                    initial_set,
                    parameter_minimums,
                    parameter_maximums,
                    all_parameters_names,
                    all_parameters_values,
                    nb_iter_optimization,
                    nb_iter_alice,
                    input_file_name="In_Pulses.txt",
                    output_file_name="Output.txt",
                    path="",
                    **kwargs) :

    '''
    This function optimizes the cold_atom experiment with the Nelder-Mead algorithm

    :param selection: list of ints, indexes of the parameters to be optimized
    :param initial_set: list of lists of floats. Each sublist represent a set of parameters values for initialisation of NM.
    :param parameter_minimums: list of the minimums of the parameters. This is used to keep the parameters values in healthy boundaries.
    :param parameter_maximums: list of the maximums of the parameters. This is used to keep the parameters values in healthy boundaries.
    :param all_parameters_names: list of strings, names of all the parameters
    :param all_parameters_values: list of floats, values of all the parameters
    :param nb_iter_optimization: number of iteration of the Nelder_Meal algorithm
    :param nb_iter_alice: number of experiments run to average the cost function corresponding to 1 set of parameters
    :param input_file_name: file name pointed to Alice
    :param output_file_name: file name pointed to Alice
    :param path: points to the directory where the above files will be created

    :return: dataframe. 2-D array of AliceParameters objects.
    An AliceParam object represents a set of parameters
    and the nb_iter_alice experiments that have been run with their values
    See more on the wiki of the repository
    '''





    dataframe, current_set= optimization_NM_init(selection,
                                              initial_set,
                                              parameter_minimums,
                                              parameter_maximums,
                                              all_parameters_names,
                                              all_parameters_values,
                                              nb_iter_alice,
                                              input_file_name=input_file_name,
                                              output_file_name=output_file_name,
                                              path=path)
    if handler.SIGINT:
        return dataframe

    for k in range(nb_iter_optimization) :

        if handler.SIGINT:
            return dataframe

        print("OPTIMISATION {}/{}".format(k+1, nb_iter_optimization))
        dataframe,current_set = optimization_NM_step(dataframe,
                                                  current_set,
                                                  selection,
                                                  initial_set,
                                                  parameter_minimums,
                                                  parameter_maximums,
                                                  all_parameters_names,
                                                  all_parameters_values,
                                                  nb_iter_alice,
                                                  input_file_name=input_file_name,
                                                  output_file_name=output_file_name,
                                                  path=path,
                                                  **kwargs)

    return dataframe
