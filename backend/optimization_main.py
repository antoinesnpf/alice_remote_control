import copy
from math import sqrt
import numpy as np
import time
from signal_handler import handler

def cost_function(parameter_values, mode = 3) :

    print('mode is ', mode)

    if mode == 0 :
        'Sphere function'

        return sum([u**2 for u in parameter_values])

    elif mode == 1 :
        'Rosenbrock function'

        res = 0

        for k in range(len(parameter_values) - 1) :

            x = parameter_values[k]
            x1 = parameter_values[k+1]
            res += 100 * (x1 - x**2)**2 + (1-x)**2

        return res

    elif mode == 2 :
        'Easom function'

        x = parameter_values[0]
        y = parameter_values[1]
        pi = np.pi
        return -np.cos(x + pi) * np.cos(y + pi) * np.exp(- x**2 - y**2) + 1

    elif mode == 3 :
        'Rastrigin noisy function'

        A = 10

        return sum([(A + x**2 + A * np.cos(2 * np.pi * x)) for x in parameter_values])

class AliceParameters :
    """
    This class receives values [parameter_values] for a selection of parameters among all the available parameters [selection, all_parameters_names, all_parameters_values].
    It then talks to Alice via the Alice feedback function to test experimentally those values [parameter_values].
    This object contains some relevant information about the experiment made with those specific values.
    """

    compteur = 0

    def __init__(self,
                 selection,
                 parameter_values,
                 parameter_minimums,
                 parameter_maximums,
                 all_parameters_names,
                 all_parameters_values,
                 nb_iter_alice,
                 label="None",
                 input_file_name="In_Pulses.txt",
                 output_file_name="Output.txt",
                 path="",
                 init=False):

        if init :
            AliceParameters.compteur = 0

        self.selection = selection
        self.minimums = parameter_minimums
        self.maximums = parameter_maximums

        l1, l2, l3, l4 =  len(self.minimums), len(self.maximums), len(parameter_values), len(selection)
        assert l1 == l2 == l3 == l4, 'Check the dimensions of your inputs in the Optimizer.'

        for k in range(len(parameter_values)) :
            if parameter_values[k] < self.minimums[k] :
                parameter_values[k] = self.minimums[k]
            elif  parameter_values[k] > self.maximums[k] :
                parameter_values[k] = self.maximums[k]

        self.names = []
        self.all_names = copy.copy(all_parameters_names)
        self.all_values = copy.copy(all_parameters_values)
        for i, index in enumerate(selection) :
            self.all_values[index] = parameter_values[i]
            self.names.append(all_parameters_names[index])

        self.label = label
        self.cd = np.array(parameter_values)
        print("new set of parameters tested.\n\tparam values : {}".format(self.cd))
        id, errors = [], []
        for k in range(nb_iter_alice) :
            AliceParameters.compteur += 1
            id.append(AliceParameters.compteur)
            
            # if you want to do fast testing uncomment this
            # errors.append(cost_function(self.all_values))
            
            # if you want to talk to Alice or its simulator, uncomment this
            errors.append(alice_feedback(self.all_names,
                                         self.all_values,
                                         input_file_name=input_file_name,
                                         output_file_name=output_file_name,
                                         path=path,
                                         init = init and k==0))

        self.id = id
        self.errors = errors
        self.nb = len(errors)
        self.mean_error = np.mean(errors)
        self.std_error = np.std(errors)
        print("\tid :{}\n\tlabel : {}\n\tmean cost : {}".format(self.id, self.label, self.mean_error))



def files_init(input_parameter_names, input_parameter_values, input_file_name="In_Pulses.txt", output_file_name="Output.txt", path="") :

    """
    This function overwrites the In_Pulse.txt with new parameters and a fresh Nit of 1
    It also creates a new blank file Output.txt for Alice to write results in
    """

    assert len(input_parameter_names)==len(input_parameter_values)

    input_file = open(path+input_file_name, 'w')

    input_file.write("Nit")
    for parameter in input_parameter_names :
        input_file.write("\tV_" + parameter)
    input_file.write("\n"+str(1))
    for value in input_parameter_values :
        input_file.write("\n" + str(value))

    input_file.close()

    output_file = open(path+output_file_name, 'w')
    output_file.write("")
    output_file.close()


def input_file_editor(input_parameter_values, input_file_name= "In_Pulses.txt", path="") :

    """
    This function edits the input_file by incrementing  nit by 1
    and editing the new values of the parameters
    """

    input_file = open(path+input_file_name, 'r')
    input_lines = input_file.readlines()
    input_file.close()

    input_file = open(path+input_file_name, 'w')
    nit = int(input_lines[1])
    input_lines[1] = str(nit + 1)+"\n"
    input_lines[2:] = [ str(parameter)+"\n" for parameter in input_parameter_values]
    input_file.writelines(input_lines)
    input_file.close()


def alice_feedback(all_parameters_names, all_parameters_values, input_file_name="In_Pulses.txt", output_file_name="Output.txt", path="", init = False) :
    '''
    This function :
    1) rewrites the input_file with the new values of the parameters
    2) waits for Alice's response by checking the output_file
    3) returns Alice's response once Alice has responded

    :param all_parameters_names: list of strings, the names of the parameters
    :param all_parameters_values: list of floats, the values of the parameters
    :param init: boolean. If True : the input file will be rewritten with a fresh Nit of 1
                          If False, the nit will be incremented by 1

    :return: The value of the cost function that Alice wrote in the Output.txt file after the experiment
    '''


    if init :
        # initialising the files

        files_init(all_parameters_names, all_parameters_values, input_file_name=input_file_name, output_file_name=output_file_name, path=path)
        nit_old_output = 0
        print("Files have been created. Waiting for Alice to start optimizing")

    else :
        # getting the old Nit(numbering of iterations)
        # and rewriting the In_Pulse.txt file
        output_file = open(path+output_file_name, 'r')
        output_lines = output_file.readlines()
        nit_old_output = int(output_lines[1])
        output_file.close()

        input_file_editor(all_parameters_values, input_file_name=input_file_name, path=path)


    # Checking if the output file has been updated by Alice

    nit_output = -1
    t0 = time.time()
    time_threshold = 100

    if init :
        time_threshold = 400

    while nit_output != nit_old_output + 1 :

        if handler.SIGINT :
            return -np.inf

        output_file = open(path+output_file_name, 'r')
        output_lines = output_file.readlines()
        if len(output_lines) >= 1 :
            nit_output = int(output_lines[1])
        output_file.close()

        time.sleep(0.5)

        if time.time() - t0 > time_threshold :
            print('Error : No response from Alice during the last {}sec'.format(time_threshold))
            return - np.inf

    # Getting the cost function value

    output_file = open(path+output_file_name, 'r')
    error = float(output_file.readlines()[2])
    output_file.close()


    return error


