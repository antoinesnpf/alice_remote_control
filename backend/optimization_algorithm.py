
import copy
from math import sqrt
import numpy as np
import time
from signal_handler import handler

### class AilceParameters definition


class AliceParameters :

    compteur = 0

    def __init__(self,
                 parameters_names,
                 parameter_values,
                 parameter_minimums,
                 parameter_maximums,
                 hyperparameters_names,
                 hyperparameters_values,
                 nb_iter_alice,
                 label="None",
                 input_file_name="In_Pulses.txt",
                 output_file_name="Output.txt",
                 path="",
                 init=False):

        self.names = parameters_names
        self.minimums = parameter_minimums
        self.maximums = parameter_maximums
        self.hypernames = hyperparameters_names
        self.hypervalues = hyperparameters_values

        l1, l2, l3, l4 = len(self.names), len(self.minimums), len(self.maximums), len(parameter_values)
        assert l1 == l2 == l3 == l4

        for k in range(len(parameter_values)) :
            if parameter_values[k] < self.minimums[k] :
                parameter_values[k] = self.minimums[k]
            elif  parameter_values[k] > self.maximums[k] :
                parameter_values[k] = self.maximums[k]

        self.label = label
        self.cd = np.array(parameter_values)

        id, errors = [], []
        for k in range(nb_iter_alice) :
            AliceParameters.compteur += 1
            id.append(AliceParameters.compteur)
            errors.append(alice_feedback(self.names, parameter_values, self.hypernames, self.hypervalues, input_file_name=input_file_name, output_file_name=output_file_name, path=path, init = init and k==0))

        self.id = id
        self.errors = errors
        self.nb = len(errors)
        self.mean_error = np.mean(errors)
        self.std_error = np.std(errors)

        print("new set of parameters tested.\n\tid :{}\n\tlabel : {}\n\tmean error : {}\n\tparam values : {}".format(self.id, self.label, self.mean_error, self.cd))


def files_init(input_parameter_names, input_parameter_values, input_file_name="In_Pulses.txt", output_file_name="Output.txt", path="") :

    """
    This function overwrites the In_Pulse.txt with new parameters and a fresh Nit of 1
    It also creates a new blank file Output.txt for Alice to write results in
    """

    assert len(input_parameter_names)==len(input_parameter_values)

    input_file = open(path+input_file_name, 'w')    #Those lines are to be placed later when so as not to overwrite to early

    input_file.write("Nit")
    for parameter in input_parameter_names :
        input_file.write("\tV_" + parameter)
    input_file.write("\n"+str(1))
    for value in input_parameter_values :
        input_file.write("\n" + str(value))

    input_file.close()

    output_file = open(path+output_file_name, 'w')
    output_file.write("")
    output_file.close()


def input_file_editor(input_parameter_values, input_file_name= "In_Pulses.txt", path="") :

    """
    This function edits the input_file by incrementing  nit by 1
    and editing the new values of the parameters
    """

    input_file = open(path+input_file_name, 'r')
    input_lines = input_file.readlines()
    input_file.close()

    input_file = open(path+input_file_name, 'w')
    nit = int(input_lines[1])
    input_lines[1] = str(nit + 1)+"\n"
    input_lines[2:] = [ str(parameter)+"\n" for parameter in input_parameter_values]
    input_file.writelines(input_lines)
    input_file.close()


def alice_feedback(parameter_names, parameter_values, hyperparameter_names, hyperparameter_values, input_file_name="In_Pulses.txt", output_file_name="Output.txt", path="", init = False) :
    '''
    This function :
    1) rewrites the input_file with the new values of the parameters
    2) waits for Alice's response by checking the output_file
    3) returns Alice's response once Alice has responded

    :param parameter_names: list of strings, the names of the parameters
    :param parameter_values: list of floats, the values of the parameters
    :param init: boolean. If True : the input file will be rewritten with a fresh Nit of 1
                          If False, the nit will be incremented by 1

    :return: The value of the cost function that Alice wrote in the Output.txt file after the experiment
    '''


    if init :
        # initialising the files

        l1 = parameter_names + hyperparameter_names
        l2 = list(parameter_values) + list(hyperparameter_values)

        files_init(l1, l2, input_file_name=input_file_name, output_file_name=output_file_name, path=path)
        nit_old_output = 0
        print("Files has been created. Waiting for Alice to start optimizing")

    else :
        # getting the old Nit(numbering of iterations)
        # and rewriting the In_Pulse.txt file
        output_file = open(path+output_file_name, 'r')
        output_lines = output_file.readlines()
        nit_old_output = int(output_lines[1])
        output_file.close()

        l2 = list(parameter_values) + list(hyperparameter_values)
        input_file_editor(l2, input_file_name=input_file_name, path=path)


    # Checking if the output file has been updated by Alice

    nit_output = -1
    t0 = time.time()
    time_threshold = 100

    if init :
        time_threshold = 400


    while nit_output != nit_old_output + 1 :

        output_file = open(path+output_file_name, 'r')
        output_lines = output_file.readlines()
        if len(output_lines) >= 1 :
            nit_output = int(output_lines[1])
        output_file.close()

        time.sleep(0.5)

        if time.time() - t0 > time_threshold :
            print('Error : No response from Alice during the last {}sec'.format(time_threshold))
            return - np.inf

    # Getting the cost function value

    output_file = open(path+output_file_name, 'r')
    error = float(output_file.readlines()[2])
    output_file.close()


    return error


def optimization_init(parameters_names,
                      initial_set,
                      parameter_minimums,
                      parameter_maximums,
                      hyperparameter_names,
                      hyperparameter_values,
                      nb_iter_alice,
                      input_file_name="In_Pulses.txt",
                      output_file_name="Output.txt",
                      path="" ) :

    initial_set = [np.array(x) for x in initial_set]
    current_set = []
    dataframe = []

    # initialisation of the scripts for Alice
    c = 1
    print("INITIALISATION {}/{}".format(c, len(initial_set)))

    x0 = initial_set[0]
    current_set.append(AliceParameters(parameters_names,
                                       x0,
                                       parameter_minimums,
                                       parameter_maximums,
                                       hyperparameter_names,
                                       hyperparameter_values,
                                       nb_iter_alice,
                                       label="init",
                                       input_file_name=input_file_name,
                                       output_file_name=output_file_name,
                                       path=path,
                                       init=True))


    for x in initial_set[1:] :

        if handler.SIGINT:
            return dataframe, current_set

        c += 1
        print("INITIALISATION {}/{}".format(c, len(initial_set)))
        current_set.append(AliceParameters(parameters_names,
                                           x,
                                           parameter_minimums,
                                           parameter_maximums,
                                           hyperparameter_names,
                                           hyperparameter_values,
                                           nb_iter_alice,
                                           label="init",
                                           input_file_name=input_file_name,
                                           output_file_name=output_file_name,
                                           path=path))

    current_set.sort(key = lambda aliceparam : aliceparam.mean_error) # sorted list of vectors according to vector.error values
    dataframe.append(copy.copy(current_set))

    return dataframe, current_set


def optimization_step(dataframe,
                      current_set,
                      parameters_names,
                      initial_set,
                      parameter_minimums,
                      parameter_maximums,
                      hyperparameter_names,
                      hyperparameter_values,
                      nb_iter_alice,
                      input_file_name="In_Pulses.txt",
                      output_file_name="Output.txt",
                      path="") :


    if handler.SIGINT:
        return dataframe, current_set

    N = len(initial_set)
    x0 = sum([vector.cd for vector in current_set[:-1]])
    x0 = x0 / (N-1)

    alpha = 1
    gamma = 2
    ro = 0.5
    sigma = 0.5

    # reflexion

    xn = current_set[-1].cd
    xr = x0+alpha*(x0-xn)
    vr = AliceParameters(  parameters_names,
                           xr,
                           parameter_minimums,
                           parameter_maximums,
                           hyperparameter_names,
                           hyperparameter_values,
                           nb_iter_alice,
                           label="reflexion",
                           input_file_name=input_file_name,
                           output_file_name=output_file_name,
                           path=path)

    if current_set[0].mean_error <= vr.mean_error and vr.mean_error < current_set[-2].mean_error :
        current_set[-1] = vr

    # expansion

    elif vr.mean_error < current_set[0].mean_error :
        xe = x0 + gamma * (xr - x0)
        ve = AliceParameters(  parameters_names,
                               xe,
                               parameter_minimums,
                               parameter_maximums,
                               hyperparameter_names,
                               hyperparameter_values,
                               nb_iter_alice,
                               label="expansion",
                               input_file_name=input_file_name,
                               output_file_name=output_file_name,
                               path=path)

        if ve.mean_error < vr.mean_error :
            current_set[-1] = ve
        else :
            current_set[-1] = vr

    # contraction

    elif vr.mean_error >= current_set[-2].mean_error :
        xc = x0 + ro * (xn - x0)
        vc = AliceParameters(  parameters_names,
                               xc,
                               parameter_minimums,
                               parameter_maximums,
                               hyperparameter_names,
                               hyperparameter_values,
                               nb_iter_alice,
                               label="contraction",
                               input_file_name=input_file_name,
                               output_file_name=output_file_name,
                               path=path)

        if vc.mean_error < current_set[-1].mean_error :
            current_set[-1] = vc

        # shrink

        else :
            for i in range(N-1) :
                xi = current_set[i].cd
                x1 = current_set[0].cd
                xi = x1 + sigma*(xi - x1)
                current_set[i] =  AliceParameters( parameters_names,
                                                   xi,
                                                   parameter_minimums,
                                                   parameter_maximums,
                                                   hyperparameter_names,
                                                   hyperparameter_values,
                                                   nb_iter_alice,
                                                   label="shrink",
                                                   input_file_name=input_file_name,
                                                   output_file_name=output_file_name,
                                                   path=path)


    current_set.sort(key = lambda aliceparam : aliceparam.mean_error)

    dataframe.append(copy.copy(current_set))

    return dataframe, current_set


def optimization(parameters_names,
                 initial_set,
                 parameter_minimums,
                 parameter_maximums,
                 hyperparameter_names,
                 hyperparameter_values,
                 nb_iter_optimization,
                 nb_iter_alice,
                 input_file_name="In_Pulses.txt",
                 output_file_name="Output.txt",
                 path="" ) :

    '''
    This function optimizes the cold_atom experiment with the Nelder-Mead algorithm

    :param parameters_names: list of strings, names of the parameters to be optimized
    :param initial_set: list of lists of floats. Each sublist represent a set of parameters values.
    :param parameter_minimums: list of the minimums of the parameters. This is used to keep the parameters values in healthy boundaries.
    :param parameter_maximums: list of the maximums of the parameters. This is used to keep the parameters values in healthy boundaries.
    :param nb_iter_optimization: number of iteration of the Nelder_Meal algorithm
    :param nb_iter_alice: number of experiments run to average the cost function corresponding to 1 set of parameters
    :param input_file_name: file name pointed to Alice
    :param output_file_name: file name pointed to Alice
    :param path: points to the directory where the above files will be created

    :return: dataframe. 2-D array of AliceParameters objects.
    An AliceParam object represents a set of parameters
    and the nb_iter_alice experiments that have been run with their values
    See more on the wiki of the repository
    '''





    dataframe, current_set= optimization_init(parameters_names,
                                              initial_set,
                                              parameter_minimums,
                                              parameter_maximums,
                                              hyperparameter_names,
                                              hyperparameter_values,
                                              nb_iter_alice,
                                              input_file_name=input_file_name,
                                              output_file_name=output_file_name,
                                              path=path)

    for k in range(nb_iter_optimization) :
        print("OPTIMISATION {}/{}".format(k+1, nb_iter_optimization))
        dataframe,current_set = optimization_step(dataframe,
                                                  current_set,
                                                  parameters_names,
                                                  initial_set,
                                                  parameter_minimums,
                                                  parameter_maximums,
                                                  hyperparameter_names,
                                                  hyperparameter_values,
                                                  nb_iter_alice,
                                                  input_file_name=input_file_name,
                                                  output_file_name=output_file_name,
                                                  path=path)

    return dataframe
