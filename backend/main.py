import sys

#sys.path.append('Z:\\experiment\\alice_remote_control\\backend')
sys.path.append('./backend/')

import signal
import numpy as np
import matplotlib.pyplot as plt
from signal_handler import handler
from summary_editor import SummaryEditor
from xml_editor import XmlEditor
from utils import random_seed
from optimization_methods import Optimizer


signal.signal(signal.SIGINT, handler.signal_handler)
handler.reset()

### settings ###

#wd = "Z:\\experiment\\DATA_ALICE - clean\\2021\\02\\2021-02-10\\06_sciRes_test_NM_opt_v3_210210\\sciRes_test_NM_opt_v3_210210\\"
wd = ".//working_directory//"


#xml_name = 'sciRes_test_NM_opt_v3_210210'
xml_name = 'sciRES_transport_test_parse_v2.xml'

xml = XmlEditor(xml_name, path=wd)
xml.get()
xml.show()
# xml.select()

selection = xml.selection
all_parameters_names = xml.hypernames
all_parameters_values = xml.hypervalues

xml.quick_select([16,23,33,35])

parameter_minimums = [30, 1.75, 20, 1]
parameter_maximums = [50, 5, 200, 5]

initial_set = [[39, 4.8, 200, 1.6], [40, 4.7, 60, 3.4], [43, 3, 60, 3.5], [44,3.5, 150, 1.8], [39, 4.8, 200, 1.6]]
# uncomment this if you want random initialization of the parameters
# initial_set = [random_seed(parameter_minimums, parameter_maximums) for k in range(3)]

nb_iter_optimization = 15 # number of iteration of the NM algorithm
nb_iter_alice = 3 # number of iteration for the computation of the cost function

### OPTIMIZATION ####

# the_mode = 'GP'for Gaussian processes, 'NM' for Nelder-Mead
the_mode = 'NM'

handler.reset()
optimizer = Optimizer(selection,
                     initial_set,
                     parameter_minimums,
                     parameter_maximums,
                     all_parameters_names,
                     all_parameters_values,
                     nb_iter_optimization,
                     nb_iter_alice,
                     path=wd)

# hyperparameters can be modified here

# this is how you change all of the hyperparameters
#hyperparameters = {"kernel" : kernel,
#                   "n_restarts_optimizer" : 10,
#                   "alpha" : alpha,
#                   "beta" : beta}

#optimizer.hyperparameters_GP = hyperparameters

# this is how you just change one of the hyperparameters
#optimizer.hyperparameters_GP["n_restarts_optimizer"] = 100

dataframe = optimizer.run(mode = the_mode)

# save everything at the end

summary_editor = SummaryEditor(dataframe, mode = the_mode, path = wd)
summary_editor.save()
summary_editor.save_plot()



###### HYPERPARAMETERS TESTING ####
# this is what was used to test all of the hyperparameters with different functions
# not needed for everyday running of the optimizer with the experiment

initial_set = [[2,-2], [-2,2]]
all_parameters_names = xml.hypernames[0:2]
all_parameters_values = xml.hypervalues[0:2]
parameter_minimums = [- 5]*2
parameter_maximums = [5]*2
initial_set = [[3,-3], [-3,3]]

les_index_des_best_points_NM = []
### optimization ###
c = 0
threshold = 0.2
NM = False

if NM == True :
    for alpha in [0.25, 0.5, 1, 1.7] :
            for gamma in [1.25, 2, 4, 10] :
                for ro in [0.1, 0.25, 0.5] :
                    for sigma in [0.2, 0.7 , 2] :
                        c+= 1

                        handler.reset()
                        optimizer = Optimizer(selection,
                                             initial_set,
                                             parameter_minimums,
                                             parameter_maximums,
                                             all_parameters_names,
                                             all_parameters_values,
                                             nb_iter_optimization,
                                             nb_iter_alice,
                                             path=wd)

                        hyperparameters = {"alpha" : alpha,
                                           "gamma" : gamma,
                                           "ro" : ro,
                                           "sigma" : sigma}

                        optimizer.hyperparameters_NM = hyperparameters

                        dataframe = optimizer.run(mode = 'NM')

                        # ploting

                        les_x = []
                        les_y = []

                        dataframe[0].sort(key = lambda AP : AP.id[0])
                        for current_point in dataframe[0] :
                            les_x.append(current_point.id[0])
                            les_y.append(current_point.mean_error)

                        for parameter_set in dataframe :
                            current_point = sorted(parameter_set, key = lambda aliceparam : aliceparam.id[0])[-1]
                            les_x.append(current_point.id[0])
                            les_y.append(current_point.mean_error)


                        les_y2 = [np.min(les_y[0:u]) for u in range(1, len(les_y) + 1)]
                        les_y3 = [0 for u in range(len(les_y))]

                        plt.figure()
                        plt.plot(les_x, les_y, "o")
                        plt.plot(les_x, les_y2, "-")
                        plt.plot(les_x, les_y3, "-")

                        plt.title('NM optimization with alpha = {}, gamma = {}, ro = {}, sigma = {}'.format(alpha, gamma, ro, sigma))
                        plt.xlabel('iteration number')
                        plt.ylabel('cost function')
                        plt.savefig(wd + 'opti_NM_{}.png'.format(c))



                        temp = []
                        for x,y in zip(les_x, les_y) :
                            if y < threshold :
                                temp.append(x)
                        if len(temp) == 0 :
                            les_index_des_best_points_NM.append(-1)
                        else :
                            les_index_des_best_points_NM.append(temp[0])

                        print("best points at etape {}:".format(c), les_index_des_best_points_NM)




GP = False
c = 0
les_index_des_best_points_GP = []

import sklearn.gaussian_process as gp
kernels = []
kernels.append(gp.kernels.ConstantKernel(1.0, (1e-1, 1e3)))
kernels.append(gp.kernels.RBF(10.0, (1e-3, 1e3)))
kernels.append(gp.kernels.ConstantKernel(1.0, (1e-1, 1e3)) * gp.kernels.RBF(10.0, (1e-3, 1e3)))
kernels.append(gp.kernels.Matern(length_scale=1.0, nu=1.5))
kernels.append(gp.kernels.ConstantKernel(1.0, (1e-1, 1e3)) * gp.kernels.Matern(length_scale=1.0, nu=1.5))
kernels.append(gp.kernels.ConstantKernel(1.0, (1e-1, 1e3)) * gp.kernels.Matern(length_scale=1.0, nu=2.5))

if GP == True :
    for kernel_nb, kernel in enumerate(kernels) :
        for alpha in [0.2, 0.6, 1, 2] :
            for beta in [0.2, 0.6, 1, 2] :
                c+= 1

                handler.reset()
                optimizer = Optimizer(selection,
                                     initial_set,
                                     parameter_minimums,
                                     parameter_maximums,
                                     all_parameters_names,
                                     all_parameters_values,
                                     nb_iter_optimization,
                                     nb_iter_alice,
                                     path=wd)

                hyperparameters = {"kernel" : kernel,
                                   "n_restarts_optimizer" : 10,
                                   "alpha" : alpha,
                                   "beta" : beta}

                optimizer.hyperparameters_GP = hyperparameters

                dataframe = optimizer.run(mode = 'GP')
                dataframe = [[u] for u in dataframe]
                # ploting

                les_x = []
                les_y = []

                dataframe[0].sort(key = lambda AP : AP.id[0])
                for current_point in dataframe[0] :
                    les_x.append(current_point.id[0])
                    les_y.append(current_point.mean_error)

                for parameter_set in dataframe :
                    current_point = sorted(parameter_set, key = lambda aliceparam : aliceparam.id[0])[-1]
                    les_x.append(current_point.id[0])
                    les_y.append(current_point.mean_error)


                les_y2 = [np.min(les_y[0:u]) for u in range(1, len(les_y) + 1)]
                les_y3 = [0 for u in range(len(les_y))]

                plt.figure()
                plt.plot(les_x, les_y, "o")
                plt.plot(les_x, les_y2, "-")
                plt.plot(les_x, les_y3, "-")

                plt.title('GP optimization with kernel nb {}, alpha = {}, beta = {}'.format(kernel_nb, alpha, beta))
                plt.xlabel('iteration number')
                plt.ylabel('cost function')
                plt.savefig(wd + 'opti_GP_{}.png'.format(c))

                temp = []
                for x,y in zip(les_x, les_y) :
                    if y < threshold :
                        temp.append(x)
                if len(temp) == 0 :
                    les_index_des_best_points_GP.append(-1)
                else :
                    les_index_des_best_points_GP.append(temp[0])

                print("best points at etape {}:".format(c), les_index_des_best_points_GP)


# print('score NM :', les_index_des_best_points_NM)
# print('score GP :', les_index_des_best_points_GP)
