import copy

class XmlEditor :

    def __init__(self, name, path='./working_directory/') :

        if name[-4:] == '.xml' :
            name = name[:-4]

        self.path = path
        self.name = name
        self.is_parsed = False
        self.selection = []
        self.hypernames = []
        self.hypervalues = []

    def show(self):

        assert self.is_parsed, 'run the .get() method first on Xml object'

        print('file: ' + self.name)

        keys = {}
        for i, key in enumerate(self.var_values.keys()) :
            print('{} | {} : {}'.format(i, key, self.var_values[key]))
            keys[i] = key

    def get(self) :

        with open(self.path + self.name + '.xml', 'r') as file:
            self.lines = file.readlines()

        self.var_names = []
        lines = copy.copy(self.lines)

        sciIndexes = []

        for i in range(len(lines)) :
            lines[i] = lines[i][0:-1]
            test = lines[i][0:5] == "<Val>"
            test = test and lines[i][-6:] == '</Val>'
            test = test and lines[i][5:-6][0:15] == self.name[0:15]
            if test :
                sciIndexes.append(i)

        sciIndex = sciIndexes[-1]

        LUTIndexes = []

        for k in range(50) :
            if lines[sciIndex + k] == '<Name>LUT Public blocks</Name>' :
                LUTIndexes.append(sciIndex + k)
        assert len(LUTIndexes) == 1, "line LUT Public blocks not found in .xml file"

        LUTIndex = LUTIndexes[0]

        phrase = []

        for k in range(LUTIndex, len(lines)) :
            if lines[k] == '<Array>' :
                phrase.append((k, '<'))
            elif lines[k] == '</Array>' :
                phrase.append((k, '>'))

        assert phrase[0][1] == '<' and phrase[1][1] == '>', 'method to determine the position of first <array>...</array> failed'

        l1 = phrase[0][0]
        l2 = phrase[1][0]

        var_names = []
        var_values = {}
        var_lines = {}

        for k in range(l1, l2) :
            test = lines[k] == '<Name>var name</Name>'
            test = test and lines[k+1][0:5] == '<Val>'
            test = test and lines[k+1][-6:] == '</Val>'
            test = test and lines[k+1][5:-6] != '' #testing if the name of the variable is not the empty string
            test = test and lines[k+4] == '<Name>var</Name>'
            test = test and lines[k+5][0:5] == '<Val>'
            test = test and lines[k+5][-6:] == '</Val>'
            test = test and lines[k+5][5:-6] != '' #testing if the value of the variable is not the empty string

            if test :
                var_name = lines[k+1][5:-6]
                var_names.append(var_name)
                var_values[var_name] = float(lines[k + 5][5:-6])
                var_lines[var_name] = k + 5

        self.var_names = var_names
        self.var_values = var_values
        self.var_lines = var_lines
        self.is_parsed = True

        return var_values

    def set(self, var_name, var_value):

        var_line = self.var_lines[var_name]
        new_line = '<Val>' + str(var_value) + '</Val>\n'

        with open(self.path + self.name + '.xml', 'w') as file:
            for i, line in enumerate(self.lines) :
                if i == var_line :
                    line = new_line
                file.write(line)

        self.var_values[var_name] = var_value
        self.lines[var_line] = new_line
        print('line {} of xml modified with value {} for variable {}'.format(var_line + 1, var_value, var_name))

    def quick_set(self, var_names, var_values): #can be optimized by not rewritting th whole file each time

        assert len(var_names) == len(var_values), "lengths of arguments don't match"

        for i in range(len(var_names)) :
            self.set(var_names[i], var_values[i])

    def select(self) :

        var_values = self.get()

        print('file: ' + self.name)

        keys = {}
        for i, key in enumerate(var_values.keys()) :
            print('{} | {} : {}'.format(i, key, var_values[key]))
            keys[i] = key

        print('select variables from the above list by \ninputing their line numbers separated by comas')
        print("once all desired variables are selected, input 'exit'")

        var_num = []
        inp = input()
        # interactive code
        while inp != 'exit' :
            while not input_to_bool(inp):
                print('Wrong input. Try again.')
                inp = input()

            if inp == 'exit' :
                break

            var_num += input_to_int(inp)
            inp = input()

        for i, key in enumerate(var_values.keys()) :
            if i in var_num :

                self.selection.append(i)

            self.hypernames.append(keys[i])
            self.hypervalues.append(var_values[keys[i]])

    def quick_select(self, var_num):

        var_values = self.get()

        keys = {}
        for i, key in enumerate(var_values.keys()) :
            keys[i] = key

        for i, key in enumerate(var_values.keys()) :
            if i in var_num :

                self.selection.append(i)

            self.hypernames.append(keys[i])
            self.hypervalues.append(var_values[keys[i]])


def input_to_bool(inp) :

    if inp == 'exit' :
        return True

    if inp == '' :
        return False

    for c in inp :
        if c not in ([str(k) for k in range(10)] + [',', ' ']) :
            return False

    return True

def input_to_int(inp) :

    L = []

    new_number = True
    for c in inp :

        if c == ',':
            new_number = True

        elif c in [str(u) for u in range(10)] :
            if new_number :
                L.append(c)
                new_number = False
            else :
                L[-1] += c

    print(L)
    return [int(u) for u in L]


#########################################################################################

test1 = False
if test1 :
    for name in ['sciRES_transport_test_parse', 'sciRES_transport_test_parse_v2', 'sciRES_transport_test_parse_v3']:

        xml = XmlEditor(name)

        print('file: ' + name)
        print('variables and values:', xml.get())
        print('\n')

test2 = False
if test2 :
    name = 'sciRES_transport_test_parse'
    xml = XmlEditor(name)
    var_names = ['good variable_1', 'good variable_2', 'good variable 3', 'good variable 4', 'good variable 5']
    xml.get()
    xml.quick_set(var_names, [1.0, 2.0, 3.0, 4.0, 5.0])

test3 = False
if test3 :
    for name in ['sciRes_dimples_3Dlatt_fluor_pifoc_scan', 'sciRes_dimples_circle_array_201105'] :
        xml = XmlEditor(name)

        print('file: ' + name)
        print('variables and values:\n')
        res = xml.get()
        for key in res.keys() :
            print('{} : {}'.format(key, res[key]))
        print('\n')


test4 = False
if test4 :
    name = 'sciRes_dimples_3Dlatt_fluor_pifoc_scan'
    xml = XmlEditor(name)
    xml.quick_select([0, 1, 2, 3, 4])


# important note. Two methods can work to get the names of the variables.
# first is : parse from the last line where 'LUT Public blocks' appears
# second is : parse from the first time 'LUT Public blocks' appears after 'sCires_*' has appeared for the last time
