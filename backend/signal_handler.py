#!/usr/bin/env python
import signal

class SIGINT_handler():
    def __init__(self):
        self.SIGINT = False

    def signal_handler(self, signal, frame):
        print('Ctrl+C detected, optimization should shut down!')
        self.SIGINT = True

    def reset(self) :
        self.SIGINT = False


handler = SIGINT_handler()
