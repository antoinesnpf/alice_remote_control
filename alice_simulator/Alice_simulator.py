import os.path
import time
import numpy as np

def cost_function(parameter_values, mode = 3) :

    mode = 2
    print('mode is ', mode)
    if mode == 0 :
        'Sphere function'

        return sum([u**2 for u in parameter_values])

    elif mode == 1 :
        'Rosenbrock function'

        res = 0

        for k in range(len(parameter_values) - 1) :

            x = parameter_values[k]
            x1 = parameter_values[k+1]
            res += 100 * (x1 - x**2)**2 + (1-x)**2

        return res

    elif mode == 2 :
        'Easom function'

        x = parameter_values[0]
        y = parameter_values[1]
        pi = np.pi
        return -np.cos(x + pi) * np.cos(y + pi) * np.exp(- x**2 - y**2) + 1

    elif mode == 3 :
        'Rastrigin noisy function'

        A = 10

        return sum([(A + x**2 + A * np.cos(2 * np.pi * x)) for x in parameter_values])




def simulation_step(input_file_name="In_Pulses.txt", output_file_name="Output.txt", path="", waiting=False, waiting_time = 3 ) :

    if os.path.exists(path+input_file_name) and os.path.exists(path+output_file_name) :

        input_file = open(path+input_file_name, 'r')
        input_lines = input_file.readlines()

        if len(input_lines)>= 2 :
            nit = int(input_lines[1])
            parameter_values=[]
            for parameter in input_lines[2:] :
                parameter_values.append(float(parameter))
        else :
            print("Error : Input file is not conform")

        input_file.close()

        error = cost_function(parameter_values)
        if waiting :
            time.sleep(waiting_time)

        output_file = open(path+output_file_name, 'w')
        output_file.write("Nit")
        output_file.write("\tFom")
        output_file.write("\n"+str(nit))
        output_file.write("\n"+str(error))
        output_file.close()


    else:
        print("Error : Missing files")

    return(error)


def run_simulation(simulation_time=20, input_file_name="In_Pulses.txt", output_file_name="Output.txt", path="", looping = False, waiting = False, waiting_time = 3) :

    """
    This function simulates an experiment in the lab by uptading Output.txt
    with an arbitrary cost function computed with the values written in In_Pusles.txt
    This function is used to test the code without being in the lab

    :param simulation_time:  number of seconds that this function will run
    :param looping : boolean. If true : the function will keep updating Output.txt as long as the function runs. If false, it will only update it once.
    :param waiting : boolean. If true : the function will wait before updating Output.txt so as to simulate the duration of the experiment.
    :param waiting_time : number of seconds to wait

    """


    if looping :

        t0 = time.time()
        while time.time() - t0 + 3 < simulation_time :
            error=simulation_step( input_file_name=input_file_name, output_file_name=output_file_name, path=path, waiting=waiting, waiting_time = waiting_time)
            print("remaining time for simulation : {} sec ".format( int(simulation_time+t0-time.time()))  )
            print("cost_func = {}".format(error))
    else :

        simulation_step( input_file_name=input_file_name, output_file_name=output_file_name, path=path, waiting_time = waiting_time)



wd = "..//working_directory//"

run_simulation(simulation_time = 36000, looping = True, waiting = True, waiting_time = 1.5, path = wd)


