from flask import Flask, render_template, request, redirect, url_for
from backend.optimization_algorithm import optimization_init, optimization_step
from flaskr.utils import dict_converter, init_data_opti
import time

app = Flask(__name__)

optimization_data = { 'nb_iter_opti': 5,
                      'nb_iter_alice': 2,
                      'nb_parameters' : 2,
                      'initial_set_length' : 5,
                      'parameter_names_and_values': [['parameter1'], ['parameter2']]}

dataframe = None


@app.route('/')
def home() :
    return render_template('home.html')

@app.route('/optimization/setup', methods=['POST', 'GET'])
def optimization_setup_view() :

    global optimization_data
    init_data_opti(optimization_data)

    if request.method == 'GET' :
        return render_template('optimization_setup.html', data = optimization_data)

    if request.method =='POST':

        # checking if the optimization button has been pressed
        if 'optimize_button' in request.form :

            return redirect(url_for('optimization_run_view'))


        # fetching nb_iter_opti, nb_iter_alice and initial_set_lenght
        else :
            optimization_data['nb_iter_opti'] = int(request.form["nb_iter_opti"])
            optimization_data['nb_iter_alice'] = int(request.form["nb_iter_alice"])
            temp = request.form["initial_set_length"]
            if int(temp) != optimization_data['initial_set_length']:
                optimization_data['initial_set_length'] = int(temp)
                init_data_opti(optimization_data)
                print(optimization_data)
                return render_template('optimization_setup.html', data = optimization_data)

            # fetching input values for parameters
            for i in range(optimization_data['nb_parameters']) :
                for j in range(1, optimization_data['initial_set_length'] + 1) :
                    temp = request.form['{},{}'.format(i, j)]
                    if temp != '' :
                        optimization_data['parameter_names_and_values'][i][j] = float(temp)

    print(optimization_data)
    return render_template('optimization_setup.html', data = optimization_data)

@app.route('/optimization/run')
def optimization_run_view(init = True, k=0) :

    global optimization_data
    global dataframe

    return render_template('optimization_run.html', data = optimization_data, results = dataframe)

    parameters_names, initial_set, parameter_minimums, parameter_maximums, nb_iter_opti, nb_iter_alice, wd = dict_converter(optimization_data)
    if init :
        dataframe, current_set = optimization_init(parameters_names,
                                                  initial_set,
                                                  parameter_minimums,
                                                  parameter_maximums,
                                                  nb_iter_alice,
                                                  input_file_name="In_Pulses.txt",
                                                  output_file_name="Output.txt",
                                                  path=wd)

    else :
        print("OPTIMISATION {}/{}".format(k+1, nb_iter_opti))
        dataframe,current_set = optimization_step(dataframe,
                                                  current_set,
                                                  parameters_names,
                                                  initial_set,
                                                  parameter_minimums,
                                                  parameter_maximums,
                                                  nb_iter_alice,
                                                  input_file_name="In_Pulses.txt",
                                                  output_file_name="Output.txt",
                                                  path=wd)

    print("df size: ", len(datagrame), len(dataframe[0]))



if __name__ ==  '__main__' :
    app.run(debug = True)

