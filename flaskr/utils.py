from backend.optimization_algorithm import optimization
import numpy as np
"""
optimization_data = { 'nb_iter_opti': 5,
                      'nb_iter_alice': 2,
                      'nb_parameters' : 2,
                      'initial_set_length' : 5,
                      'parameter_names_and_values': [['parameter1'], ['parameter2']]}
"""

def init_data_opti(optimization_data) :

    for parameter_vector in optimization_data['parameter_names_and_values']:
       for k in range( optimization_data['initial_set_length'] - len(parameter_vector) + 1) :
            parameter_vector.append('')
       for k in range( - optimization_data['initial_set_length'] + len(parameter_vector) - 1) :
            parameter_vector.pop()


def dict_converter(optimization_data) :

    l=len(optimization_data['parameter_names_and_values'])
    nb_parameters = optimization_data['nb_parameters']
    initial_set_length = optimization_data['initial_set_length']

    assert l == nb_parameters, "supposed to be {} parameters but {} lines in frontend".format(nb_parameters, l)

    for k in range(nb_parameters):
        l = len(optimization_data['parameter_names_and_values'][k])
        assert  l == initial_set_length + 1, "frontend data is corrupted\nat line {} : {} values but expected {} values".format(k, l - 1 , initial_set_length)

    nb_iter_opti = optimization_data['nb_iter_opti']
    nb_iter_alice = optimization_data['nb_iter_alice']

    input_parameter_names = []
    for vector in optimization_data['parameter_names_and_values'] :
        input_parameter_names.append(vector[0])

    initial_set = []
    for k in range(initial_set_length) :
        initial_set.append([])

    for i in range(nb_parameters) :
        for j in range(initial_set_length) :

            initial_set[j].append(optimization_data['parameter_names_and_values'][i][j+1])

    parameter_minimums = [-np.inf] * nb_parameters
    parameter_maximums = [np.inf] * nb_parameters

    wd = "..//working_directory//"

    return input_parameter_names, initial_set, parameter_minimums, parameter_maximums, nb_iter_opti, nb_iter_alice, wd

test = False
if test :
    d = {'nb_iter_opti': 5,
     'nb_iter_alice': 2,
     'initial_set_lenght': 5,
     'nb_parameters': 3,
     'parameter_names_and_values': [['parameter1', 0.0, 1.0, 2.0, 3.0, 4.0],
                                    ['parameter2', 5.0, 6.0, 7.0, 8.0, 9.0],
                                    ['parameter3', 5.0, 6.0, 7.0, 8.0, 9.0]]}

    print(dict_converter((d)))
