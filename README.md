
*This project is made for the experimentalists of the Quantum Physics department of Aarhus University, Denmark.*

*If you have any issues, check the wiki here on Gitlab*



**To set up the remote control of Alice :**

1. Clone this repository

2. Open `main.py`

3. Set the variable `wd` to the working directory of your choice

4. Choose a set of parameters for the experiment. Set the following :
- `parameters_names` (note : right now, only one set of parameter can be used because of xml files generation.)
- `parameters_maximums` 
- `parameters_minimum`
- `initial_set` 

5. Choose the values of
- `nb_iter_optimization` (number of iteration of the Nelder-Meal algorithm) 
- `nb_iter_alice` (number of averaging points for the computation of the error)


6. In the Alice interface : 
- give the right xml file to Alice (note : right now, only one xml file is available for a specific set of paramaters. In the future, for any set of parameter, the associated xml file would be generated)
- generate a perfect_fit file (.txt) and give it to Alice
- point to Alice the In_Pulses.txt and Output.txt files
- point to Alice the perfect_fit_return file
- set the mode to 'remote optimisation' 
- set the benchmark to "every 10 runs"

7. Execute the function ```optimisation(parameters_name, 
                                        parameters_minimum, 
                                        parameters_maximum, 
                                        nb_iter_optimization, 
                                        nb_iter_alice, 
                                        path = wd)``` 

8. Start the optimisation in the Alice interface
